
<?php
	if( isset($_GET['e']) && strlen($_GET['e']) > 0 ) {
        $e = htmlspecialchars(trim($_GET['e']));
    }else{
        $e = 'fachada-1';
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>Tour Virtual | Residence</title>
	<meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<meta http-equiv="x-ua-compatible" content="IE=edge" />

	<link rel="shortcut icon" type="image/x-icon" href="../assets/images/favico.ico">
	<link rel="apple-touch-icon" href="../assets/images/phone.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../assets/images/tablet.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../assets/images/retina.png">

	<style>
		@-ms-viewport { width:device-width; }
		@media only screen and (min-device-width:800px) { html { overflow:hidden; } }
		html { height:100%; }
		body { height:100%; overflow:hidden; margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#FFFFFF; background-color:#000000; }
	</style>
</head>
<body>

<script src="tour.js"></script>

<div id="pano" style="width:100%;height:100%;">
	<noscript><table style="width:100%;height:100%;"><tr style="vertical-align:middle;"><td><div style="text-align:center;">ERROR:<br/><br/>Javascript not activated<br/><br/></div></td></tr></table></noscript>
	<script>
		embedpano({swf:"tour.swf", xml:"tour.xml", target:"pano", html5:"prefer", passQueryParameters:true, vars:{escena:"<?= $e; ?>"}});
	</script>
</div>

</body>
</html>
